import Vue from 'vue'
import App from './App.vue'
import {ServerTable, ClientTable} from 'vue-tables-2';
import axios from 'axios';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

Vue.prototype.$axios = axios
Vue.use(ServerTable);
Vue.use(ClientTable);
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
