const express = require('express');
const cors = require('cors');
const app = express();
const bodyparser = require("body-parser");
const port = process.env.PORT||3200;
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));
app.use(cors());
app.options('*', cors());
app.get('/getData', (req, res) => {

    const company = req.query.company;
    const orderBy = req.query.orderBy;
    const sort = req.query.sort;
    const page = req.query.page;

    const array = [
        {
            id: 1,
            start_date: '20/01/2019',
            time_zone: 'Europe',
            status: 'live',
            company: 'test'
        },
        {
            id: 2,
            start_date: '20/01/2029',
            time_zone: 'Europe',
            status: 'live',
            company: 'testd'
        },
        {
            id: 3,
            start_date: '20/01/2019',
            time_zone: 'berlin',
            status: 'precast',
            company: 'test'
        },
        {
            id: 4,
            start_date: '20/01/2019',
            time_zone: 'Europe',
            status: 'live',
            company: 'grtd'
        },
        {
            id: 5,
            start_date: '20/01/2018',
            time_zone: 'ofdfs',
            status: 'live',
            company: 'test'
        },
        {
            id: 6,
            start_date: '20/03/2019',
            time_zone: 'Europe',
            status: 'finished',
            company: 'tester'
        },
        {
            id: 7,
            start_date: '10/01/2019',
            time_zone: 'Europe',
            status: 'live',
            company: 'qwwed'
        },
        {
            id: 8,
            start_date: '20/01/2000',
            time_zone: 'newyork',
            status: 'live',
            company: 'tata'
        },
        {
            id: 9,
            start_date: '20/01/2009',
            time_zone: 'afganistan',
            status: 'live',
            company: 'test'
        },
        {
            id: 10,
            start_date: '10/01/2019',
            time_zone: 'Europe',
            status: 'live',
            company: 'testfd'
        }
    ]
    let curPageData = [];
    let start = 0;
    let end = 5;
    for(let i=0; i<(array.length / 5);i++) {
        curPageData[i] = array.slice(start, end);
        start += 5;
        end += 5;
    }
    const curPage = curPageData[+page - 1];
    const filterCompanyData = curPage.filter(item => item.company.includes(company));
    const resArray = filterCompanyData.length ? filterCompanyData : curPage;
    console.log(orderBy)
    if (orderBy) {
        resArray.sort((a, b) => {
           return a[orderBy] < b[orderBy] ? (sort === 'asc' ? -1 : 1) : (sort === 'asc' ? 1 : -1);
        });
    }
    res.status(200).send(resArray);
});
app.listen(port, () => {
 console.log('connected');
});
